# Contributing

```shell
git clone https://gitlab.com/elioway/elioangels.git
cd elioangels
git submodule init
git submodule update
# or
set L chisel daemon generator-art say
for R in $L
    git submodule add git@gitlab.com:elioangels/$R.git
end
```

## TODOS

1. Add a command line app called **tew**
