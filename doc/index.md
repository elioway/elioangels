<aside>
  <dl>
  <dd>Paved after him a broad and beaten way</dd>
  <dd>Over the dark Abyss, whose boiling gulf</dd>
  <dd>Tamely endured a bridge of wondrous length,</dd>
  <dd>From Hell continued, reaching th' utmost orb</dd>
  <dd>Of this frail World; by which the Spirits perverse</dd>
  <dd>With easy intercourse pass to and fro</dd>
  <dd>To tempt or punish mortals, except whom</dd>
  <dd>God and good Angels guard by special grace</dd>
</dl>
</aside>

# Of rebel Angels

Apps, libraries, modules or projects intended for use by **elioWay** developers.

They are utilities which can be dependencies of other **elioWay** apps, or just tools in the kit for building them; but generally they shouldn't be dependent on **elioWay** projects outside of **elioangels**.

Generally speaking, the apps and modules here are functional tools which have helped progress this project in one way or another, but we're leading toward creating a single purpose, but modular CLI application call **TEW**.

## Featured

<section>
  <figure>
  <a href="/elioangels/generator-angels/artwork/splash.jpg" target="_splash"><img src="/elioangels/generator-angels/artwork/splash.jpg" target="_splash"></a>
  <h3>Repos</h3>
  <p>Boilerplate artwork, doc, READMEs, + stuff you need to scaffold applications, the <strong>elioWay</strong>.</p>
  <p><a href="/elioangels/generator-angels/"><button><img src="/elioangels/generator-angels/favicoff.png"/>generator-angels</button></a></p>
</figure>
  <figure>
  <a href="/elioangels/chisel/artwork/splash.jpg" target="_splash"><img src="/elioangels/chisel/artwork/splash.jpg" target="_splash"></a>
  <h3>Document</h3>
  <p>A way to build documentation, the <strong>elioWay</strong>.</p>
  <p><a href="/elioangels/chisel/"><button><img src="/elioangels/chisel/favicoff.png"/>chisel</button></a></p>
</figure>
  <figure>
  <a href="/elioangels/generator-art/artwork/splash.jpg" target="_splash"><img src="/elioangels/generator-art/artwork/splash.jpg" target="_splash"></a>
  <h3>Art</h3>
  <p>Artwork, the <strong>elioWay</strong>.</p>
  <p><a href="/elioangels/generator-art/"><button><img src="/elioangels/generator-art/favicoff.png"/>generator-art</button></a></p>
</figure>
</section>
