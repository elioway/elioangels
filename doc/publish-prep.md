# Publishing Usage

## Full cycle

Uninstall all glocal `@elioway` node packages.

```
for R in (npm list -g)
  set LINE (string replace "├── " "" $R)
  set LINE (string replace " -> " "|" $LINE)
  set NAME (string split "|" $LINE)[1]
  set ELIO (string sub -s 1 -l 8 $NAME)
  if test (string match -e "@elioway" $ELIO)
    echo $NAME
  #  npm uninstall -g $N
  end
end

npm uninstall -g @elioway/adon @elioway/bones @elioway/chisel @elioway/dbhell @elioway/eve @elioway/god @elioway/icon @elioway/innocent @elioway/sassy-fibonacciness  @elioway/thing generator-angels  generator-sin generator-thing


npm uninstall -g @elioway/chisel @elioway/bones @elioway/thing generator-angels generator-sin generator-thing
```

Remove generated folders.

```
rm -rf **/node_modules
rm -rf **/public
```

Remove generated artwork.

```
find . -type f -name "apple-touch-icon.png" -delete
find . -type f -name "elio-*-logo.png" -delete
find . -type f -name "star.png" -delete
find . -type f -name "postcard.jpg" -delete
find . -type f -name "favicoff.png" -delete
find . -type f -name "favicon.png" -delete
find . -type f -name "favicon.png" -delete
```

Generate artwork.

```
elio-art elioway
```

Generate public.

```
set GRP elioAll
set MSG "dogma"
gita super $GRP add .
gita super $GRP commit -m $MSG
gita super $GRP push

set L elioangels eliobones elioflesh eliosin eliothing

cd ~/Dev/elioway/

for R in $L
    cd $R
    chisel
    cd ..
end
chisel
gulp

Put all `eliosin/icon/`  folders `eliosin/public/icon/ `

 rsync -zarv --progress --exclude="*.json"  --exclude="*.md"  --exclude=".*"   --exclude="*.yaml" --exclude="*.svg" ./eliosin/icon ./eliosin/public --



set GRP elioCore
gita super $GRP add .
gita super $GRP commit -m $MSG
gita super $GRP push
```

Link to chisel while developing:

```
cd ~/Dev/elioway/elioangels/chisel
npm link

cd ~/Dev/elioway/
npm i @elioway/chisel -D
for D in elioangels eliobones elioflesh eliosin eliothing
  cd $D
  # npm link @elioway/chisel
  npm i @elioway/chisel -D
  cd ..
end
```
